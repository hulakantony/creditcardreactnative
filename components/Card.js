import React, { Component } from 'react';
import { StyleSheet, View, ImageBackground, Text, Image, ActivityIndicator } from 'react-native';
import Orientation from 'react-native-orientation';
import numberImages from '../data/numbersImageSource';
import { cardsRequestPath } from '../constants/config';

export default class CardBackground extends Component {
  static navigationOptions = {
    title: 'Credit card',
  };
  state = {
    cardData: null
  }
  componentDidMount() {
    Orientation.lockToLandscape(); // makes landscape orientation for better readability

    // api server request emulation with delay
    this._sleep(2000).then(() => {
      this.fetchCreditCards()
        .then(cards => {
          const randomCardData = this._getRandomCardData(cards);
          this.setState({
            cardData: randomCardData
          });
        })
    })
  }
  componentWillUnmount() {
    Orientation.lockToPortrait();
  }
  fetchCreditCards = () => {
    return fetch(cardsRequestPath)
      .then(response => response.json())
      .then(data => data)
      .catch(error => {
        console.error(error);
      });
  }
  _getRandomCardData = (cards) => {
    return cards[Math.floor(Math.random() * cards.length)];
  }
  _sleep = (time) => {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve()
      }, time)
    })
  }
  _splitArrayIntoChunks = (array, size) => {
    const results = [];
    while (array.length) {
      results.push(array.splice(0, size));
    }
    return results;
  }
  renderCardContent = (cardData) => {
    const { pan_number, expiration_month, expiration_year, cvv } = cardData;
    const panNumberChunks = this._splitArrayIntoChunks(pan_number.split(''), 4);
    const [yearDecade, year] = expiration_year.split('').slice(2);
    const [monthFirstNumber, monthSecondNumber] = expiration_month;
    return (
      <React.Fragment>
        <View style={styles.numbersContainer}>
          {
            panNumberChunks.map((numbers, index) => {
              return <View 
                key={index} 
                style={[styles.numbersChunk, { marginRight: index !== panNumberChunks.length - 1 ? 13 : 0 }]}>
                {
                  numbers.map((number, index) => {
                    return <Image
                      key={index}
                      style={styles.panNumberImage} 
                      source={numberImages[number].file}>
                    </Image>
                  })
                }
              </View>
            })
          }
        </View>
        <View style={styles.expirationDateWrapper}>
          <Image style={styles.dateNumberImage} source={numberImages[monthFirstNumber].file}/>
          <Image style={styles.dateNumberImage} source={numberImages[monthSecondNumber].file}/>
          <Text style={styles.dateSeparator}>/</Text>
          <Image style={styles.dateNumberImage} source={numberImages[yearDecade].file}/>
          <Image style={styles.dateNumberImage} source={numberImages[year].file}/>
        </View>
        <View style={styles.cvvWrapper}>
          <Text style={styles.cvvNumberText}>CVV {cvv}</Text>
        </View>
      </React.Fragment>
    )
  }
  render() {
    const { cardData } = this.state;
    return (
      <View style={styles.cardLayout}>
        <View style={styles.backgroundImageWrapper}>
          <ImageBackground 
            source={require('../assets/images/Payment-card-background.png')}
            style={[styles.backgroundImage, { opacity: !cardData ? 0.5 : 1 }]}
          >
            {
              !cardData ?
              <ActivityIndicator size='large'/> :
              this.renderCardContent(cardData)
            }
          </ImageBackground>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardLayout: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  backgroundImageWrapper: {
    width: '82%',
    height: '100%',
  },
  backgroundImage: {
    flex: 1,
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
  },
  numbersContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 60,
    width: 100,
  },
  numbersChunk: {
    flexDirection: 'row',
  },
  panNumberImage: {
    height: 35,
    width: 23,
    resizeMode: 'contain'
  },
  dateNumberImage: {
    height: 22,
    width: 15,
    resizeMode: 'contain',
    marginRight: 0
  },
  expirationDateWrapper: {
    flexDirection: 'row',
    position: 'absolute',
    justifyContent: 'flex-start',
    alignItems: 'center',
    bottom: 60,
    left: 170
  },
  dateSeparator: {
    color: '#ccc',
    fontSize: 25
  },
  cvvWrapper: {
    position: 'absolute',
    left: 270,
    bottom: 60
  },
  cvvNumberText: {
    color: '#fff',
    fontSize: 20
  }
})