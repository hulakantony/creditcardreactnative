import React, {Component} from 'react';
import {StyleSheet, Text, View, Button} from 'react-native';
import Orientation from 'react-native-orientation';

export default class App extends Component {
  static navigationOptions = {
    title: 'Home',
  };
  componentDidMount() {
    Orientation.lockToPortrait(); // lock portrait orientation on this screen
  }
  componentWillUnmount() {
    Orientation.unlockAllOrientations();
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Button title="Get my card" onPress={() => {
          navigate('Card')
        }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});