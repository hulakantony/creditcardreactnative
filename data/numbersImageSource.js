export default {
  '0': {
    title: '0',
    file: require('../assets/images/numbers/0.png'),
  },
  '1': {
    title: '1',
    file: require('../assets/images/numbers/1.png'),
  },
  '2': {
    title: '2',
    file: require('../assets/images/numbers/2.png'),
  },
  '3': {
    title: '3',
    file: require('../assets/images/numbers/3.png'),
  },
  '4': {
    title: '4',
    file: require('../assets/images/numbers/4.png'),
  },
  '5': {
    title: '5',
    file: require('../assets/images/numbers/5.png'),
  },
  '6': {
    title: '6',
    file: require('../assets/images/numbers/6.png'),
  },
  '7': {
    title: '7',
    file: require('../assets/images/numbers/7.png'),
  },
  '8': {
    title: '8',
    file: require('../assets/images/numbers/8.png'),
  },
  '9': {
    title: '9',
    file: require('../assets/images/numbers/9.png'),
  },
}