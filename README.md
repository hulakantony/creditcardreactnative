# CreditCardReactNative

# SETUP AND RUN FLOW

1. Clone this repo `git clone https://hulakantony@bitbucket.org/hulakantony/creditcardreactnative.git`
2. Go to the project folder `cd creditcardreactnative`
3. Install all packages `npm install`
4. Run ios version `react-native run-ios`

**Note**
If your computer does not have x-code installed, please follow [React Native x-code installation guide!](https://facebook.github.io/react-native/docs/getting-started.html) tab `Building Projects with Native Code`
