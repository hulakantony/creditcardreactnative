import {
  createStackNavigator,
} from 'react-navigation';
import HomeScreen from './components/HomeScreen';
import Card from './components/Card';

const App = createStackNavigator({
  Home: { screen: HomeScreen },
  Card: { screen: Card },
});

export default App;
